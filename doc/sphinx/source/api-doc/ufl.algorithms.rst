ufl.algorithms package
======================

Submodules
----------

ufl.algorithms.ad module
------------------------

.. automodule:: ufl.algorithms.ad
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.analysis module
------------------------------

.. automodule:: ufl.algorithms.analysis
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.apply_algebra_lowering module
--------------------------------------------

.. automodule:: ufl.algorithms.apply_algebra_lowering
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.apply_derivatives module
---------------------------------------

.. automodule:: ufl.algorithms.apply_derivatives
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.apply_function_pullbacks module
----------------------------------------------

.. automodule:: ufl.algorithms.apply_function_pullbacks
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.apply_geometry_lowering module
---------------------------------------------

.. automodule:: ufl.algorithms.apply_geometry_lowering
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.apply_integral_scaling module
--------------------------------------------

.. automodule:: ufl.algorithms.apply_integral_scaling
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.apply_restrictions module
----------------------------------------

.. automodule:: ufl.algorithms.apply_restrictions
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.argument_dependencies module
-------------------------------------------

.. automodule:: ufl.algorithms.argument_dependencies
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.change_to_reference module
-----------------------------------------

.. automodule:: ufl.algorithms.change_to_reference
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.check_arities module
-----------------------------------

.. automodule:: ufl.algorithms.check_arities
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.check_restrictions module
----------------------------------------

.. automodule:: ufl.algorithms.check_restrictions
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.checks module
----------------------------

.. automodule:: ufl.algorithms.checks
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.compute_form_data module
---------------------------------------

.. automodule:: ufl.algorithms.compute_form_data
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.domain_analysis module
-------------------------------------

.. automodule:: ufl.algorithms.domain_analysis
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.elementtransformations module
--------------------------------------------

.. automodule:: ufl.algorithms.elementtransformations
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.estimate_degrees module
--------------------------------------

.. automodule:: ufl.algorithms.estimate_degrees
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.expand_compounds module
--------------------------------------

.. automodule:: ufl.algorithms.expand_compounds
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.expand_indices module
------------------------------------

.. automodule:: ufl.algorithms.expand_indices
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.formdata module
------------------------------

.. automodule:: ufl.algorithms.formdata
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.formfiles module
-------------------------------

.. automodule:: ufl.algorithms.formfiles
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.formtransformations module
-----------------------------------------

.. automodule:: ufl.algorithms.formtransformations
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.forward_ad module
--------------------------------

.. automodule:: ufl.algorithms.forward_ad
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.map_integrands module
------------------------------------

.. automodule:: ufl.algorithms.map_integrands
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.multifunction module
-----------------------------------

.. automodule:: ufl.algorithms.multifunction
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.pdiffs module
----------------------------

.. automodule:: ufl.algorithms.pdiffs
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.predicates module
--------------------------------

.. automodule:: ufl.algorithms.predicates
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.renumbering module
---------------------------------

.. automodule:: ufl.algorithms.renumbering
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.replace module
-----------------------------

.. automodule:: ufl.algorithms.replace
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.signature module
-------------------------------

.. automodule:: ufl.algorithms.signature
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.transformer module
---------------------------------

.. automodule:: ufl.algorithms.transformer
    :members:
    :undoc-members:
    :show-inheritance:

ufl.algorithms.traversal module
-------------------------------

.. automodule:: ufl.algorithms.traversal
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ufl.algorithms
    :members:
    :undoc-members:
    :show-inheritance:
